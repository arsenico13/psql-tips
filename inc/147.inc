    <div class="tip">
        The <code>\o</code> meta command will reset query output to the standard output.
      <pre><code class="hljs bash">laetitia=# \o out.out
laetitia=# select * from test limit 5;
laetitia=# \o
laetitia=# select * from test limit 5;
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
(5 rows)
</code></pre>This feature is available
at least since Postgres 7.1.
	</div>
